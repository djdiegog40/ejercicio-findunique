/*
desarrollar un script sencillo que permita obtener el número único dentro de un array
*/

function findUnique(array) {

  let mapUnique = new Map();
  array.forEach(element => {
    if (mapUnique.has(element)) {
      mapUnique.set(element, mapUnique.get(element) + 1);
    } else {
      mapUnique.set(element, 1);
    }
  });

  for (let [key, value] of mapUnique.entries()) {
    if (value === 1)
      return key;
  }
  console.log(mapUnique);
}


function check(value, expectedValue) {
  document.write(value === expectedValue ? 'PASSED' : 'FAILED');
  document.write('<br><br>');
}


check(findUnique([1]), 1);
check(findUnique([1, 1, 2]), 2);
check(findUnique([0, 0, 1, 2, 1]), 2);
check(findUnique([0, 1, 1, 2, 3, 2, 3]), 0);